package net.optebiz.crudapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import net.optebiz.crudapi.model.TblOpsReq;
import net.optebiz.crudapi.model.TblOpsReqSeg;

@Repository
public interface TblOpsReqRepository extends JpaRepository<TblOpsReq,Integer> {
@Query(nativeQuery = true)
public Optional<TblOpsReq> getTblOpsReqById(int AutoId);


//@Query(nativeQuery = true)
//public List<TblOpsReq> getTblOpsReqByIdTest();


}
