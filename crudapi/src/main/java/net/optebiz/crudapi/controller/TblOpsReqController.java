package net.optebiz.crudapi.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



import net.optebiz.crudapi.exception.BaseException;
import net.optebiz.crudapi.model.TblOpsReq;
import net.optebiz.crudapi.model.TblOpsReqSeg;
import net.optebiz.crudapi.services.TblOpsReqService;


@RestController
@RequestMapping(path = "/tblOpsReq")
public class TblOpsReqController {

	private static final Logger logger = LoggerFactory.getLogger(TblOpsReqController.class);
	
	@Autowired
	private TblOpsReqService tblOpsReqService;

	/*
	 * @GetMapping("/getDetailByAutoId/AutoId_No/{AutoId}") public
	 * Optional<TblOpsReq> getTblOpsReqById(@PathVariable("AutoId") int autoId) {
	 * logger.info("controller class ----> getTblOpsReqById method "); return
	 * tblOpsReqService.getTblOpsReqById(autoId); }
	 * 
	 * @RequestMapping(value="/getTblReqResDetails/AutoId_No/{AutoId}",method =
	 * RequestMethod.GET) public Optional<TblOpsReqSeg>
	 * getTblReqResDetails(@PathVariable("AutoId") int autoId) {
	 * logger.info("controller class ----> getTblReqResDetails method ");
	 * 
	 * return tblOpsReqService.getTblOpsSeqByAutoId(autoId); }
	 */
	
	// get details by ops req 
	
	@RequestMapping(value = "/getTblOpsReqByAutoId/{AutoId}", method = RequestMethod.GET) 
	public  Object  getTblOpsReqByAutoId(@PathVariable("AutoId") Integer autoId)  throws BaseException
	{
		if (autoId == null || autoId == 0)
		{
			 throw new BaseException("NULL and ZERO value not accept.");  
		}
		logger.info("controller class ----> getTblOpsReqByAutoId method");
		return tblOpsReqService.getTblOpsReqByAutoId(autoId);
	}
	
	
	@RequestMapping(value= "/getTblOpsReqSeqByAutoId/{AutoId}" ,  method = RequestMethod.GET) 
	public  Object  getTblOpsReqSeqByAutoId(@PathVariable("AutoId") Integer autoId)  throws BaseException
	{
		if (autoId == null || autoId == 0)
		{
			 throw new BaseException("NULL and ZERO value not accept.");  
		}
		logger.info("controller class ----> getTblOpsReqSeqByAutoId method");
		
		
		return tblOpsReqService.getTblOpsReqSeqByAutoId(autoId);
	}
	
	@RequestMapping(value= "/getMatlDefPropByAutoId/{AutoId}" ,  method = RequestMethod.GET) 
	public  Object  getMatlDefPropByAutoId(@PathVariable("AutoId") Integer autoId)  throws BaseException
	{
		if (autoId == null || autoId == 0)
		{
			 throw new BaseException("NULL and ZERO value not accept.");  
		}
		logger.info("controller class ----> getMatlDefPropByAutoId method");
		return tblOpsReqService.getMatlDefPropByAutoId(autoId);
	}
	
	@RequestMapping(value= "/getTblLineDefByAutoId/{AutoId}" ,  method = RequestMethod.GET) 
	public  Object  getTblLineDefByAutoId(@PathVariable("AutoId") Integer autoId)  throws BaseException
	{
		if (autoId == null || autoId == 0)
		{
			 throw new BaseException("NULL and ZERO value not accept.");  
		}
		logger.info("controller class ----> getTblLineDefByAutoId method");
		return tblOpsReqService.getTblLineDefByAutoId(autoId);
	}
	
	@RequestMapping(value= "/getAutoidFromOpsReqTbl" ,  method = RequestMethod.GET) 
	public  Object  getAutoidFromOpsReqTbl()  throws BaseException
	{
		
		logger.info("controller class ----> getAutoidFromOpsReqTbl method");
		return tblOpsReqService.getAutoidFromOpsReqTbl();
	}
	
	

	/*
	 * @GetMapping("/getDataByAutoIdWithParamAutoId/{AutoId}") public
	 * List<TblOpsReq> getDataByAutoIdWithParam(@PathVariable("AutoId") int autoId)
	 * { logger.info("controller class ----> getDataByAutoIdWithParam method ");
	 * return tblOpsReqService.getDataServiceByAutoIdWithParam(autoId); }
	 */
	

}
