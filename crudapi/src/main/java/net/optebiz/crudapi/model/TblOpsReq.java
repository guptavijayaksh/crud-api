package net.optebiz.crudapi.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tblOpsReq")
@NamedNativeQueries({
		@NamedNativeQuery(name = "TblOpsReq.getTblOpsReqById", query = "select AutoId ,Description,Id,Properties,ERPSeqNumber,Qty,UUId from tblOpsReq "
				+ "where AutoId = ?", resultClass = TblOpsReq.class),
		@NamedNativeQuery(name = "TblOpsReq.getTblOpsReqByIdTest", query = "select AutoId ,Description,Id,Properties,ERPSeqNumber,Qty,UUId from tblOpsReq  tblopsreq"
				+ "  join tblopsreq.listTblOpsReqSeg "),
		})

public class TblOpsReq {
	@Id
	@GeneratedValue
	@Column(name = "autoid")
	private int AutoId;
	private String Description;
	private String Id;
	private String Properties;
	private int ERPSeqNumber;
	private double Qty;
	private String UUId;

	
//	// for one  to many  
//	@OneToMany(mappedBy = "tblOpsReq", fetch = FetchType.LAZY,
//    cascade = CascadeType.ALL)
//	private List<TblOpsReqSeg> listTblOpsReqSeg;
//	// one  to many 
	
	
	public TblOpsReq() {

	}
	
	
	public TblOpsReq(String Description, String Id, String Properties) {
		this.Description = Description;
		this.Id = Id;
		this.Properties = Properties;
	}
	
	/*
	 * another constructor 
	 */
	
//	public TblOpsReq(String Description, String Id, String Properties, List<TblOpsReqSeg> tblOpsReqSeg) {
//		this.Description = Description;
//		this.Id = Id;
//		this.Properties = Properties;
//		this.listTblOpsReqSeg = tblOpsReqSeg;
//	}

	
	

//	public List<TblOpsReqSeg> getListTblOpsReqSeg() {
//		return listTblOpsReqSeg;
//	}
//
//	public void setListTblOpsReqSeg(List<TblOpsReqSeg> listTblOpsReqSeg) {
//		this.listTblOpsReqSeg = listTblOpsReqSeg;
//	}

	public void setDescription(String description) {
		Description = description;
	}



	public int getAutoId() {
		return AutoId;
	}

	public void setAutoId(int AutoId) {
		this.AutoId = AutoId;
	}

	public String getDescription() {
		return Description;
	}

	public void setDesciption(String Description) {
		this.Description = Description;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public String getProperties() {
		return Properties;
	}

	public void setProperties(String Properties) {
		this.Properties = Properties;
	}

	public int getERPSeqNumber() {
		return ERPSeqNumber;
	}

	public void setERPSeqNumber(int ERPSeqNumber) {
		this.ERPSeqNumber = ERPSeqNumber;
	}

	public double getQty() {
		return Qty;
	}

	public void setQty(double Qty) {
		this.Qty = Qty;

	}

	public String getUUId() {
		return UUId;
	}

	public void setUUId(String UUId) {
		this.UUId = UUId;
	}

	@Override
	public String toString() {
		return "TblOpsReq [AutoId=" + AutoId + ", Description=" + Description + ",Id=" + Id + ",Properties="
				+ Properties + ",ERPSeqNumber=" + ERPSeqNumber + ",UUId=" + UUId + "]";
	}
}
