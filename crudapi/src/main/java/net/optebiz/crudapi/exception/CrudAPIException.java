package net.optebiz.crudapi.exception;

public class CrudAPIException extends RuntimeException {
	private static final long serialVersionUID = -1711127190070978724L;

	public CrudAPIException(Throwable ex) {
		super(ex.getMessage(), ex);
	}
}
