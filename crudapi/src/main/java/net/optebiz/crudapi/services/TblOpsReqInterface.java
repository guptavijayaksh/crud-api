package net.optebiz.crudapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import net.optebiz.crudapi.model.TblOpsReq;
import net.optebiz.crudapi.model.TblOpsReqSeg;

public interface TblOpsReqInterface {
	
//	Optional<TblOpsReq> getTblOpsReqById(int autoId);
//	List<TblOpsReq> getTblOpsReqByIdTest();
//	Optional<TblOpsReqSeg> getTblOpsSeqByAutoId(int autoId);
	Object getTblOpsReqByAutoId(int autoId);
	Object getTblLineDefByAutoId(int autoId);
	Object getTblOpsReqSeqByAutoId(int autoId);
	Object getMatlDefPropByAutoId(int autoId);
	Object getAutoidFromOpsReqTbl();
//	List<TblOpsReq> getDataServiceByAutoIdWithParam(int autoId);
}
