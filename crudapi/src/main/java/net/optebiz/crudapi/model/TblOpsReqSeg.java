package net.optebiz.crudapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

@Entity
@Table(name = "tblOpsReqSeg")
@NamedNativeQueries({
@NamedNativeQuery(name = "TblOpsReqSeg.getTblOpsSeqByAutoId", query = "select AutoId, Description from tblOpsReqSeg where AutoId = ?", resultClass = TblOpsReqSeg.class) })
public class TblOpsReqSeg {

	@Id
	@GeneratedValue
	@Column(name = "autoid")
	private int AutoId;
	private String Description;

	public TblOpsReqSeg() {

	}

	public TblOpsReqSeg(String Description) {
		this.Description = Description;
	}

	public int getAutoId() {
		return AutoId;
	}

	public void setAutoId(int AutoId) {
		this.AutoId = AutoId;
	}

	public String getDescription() {
		return Description;
	}

	public void setDesciption(String Description) {
		this.Description = Description;
	}

	@Override
	public String toString() {
		return "TblOpsReqSeg [AutoId=" + AutoId + ", Description=" + Description + "]";

	}

}
