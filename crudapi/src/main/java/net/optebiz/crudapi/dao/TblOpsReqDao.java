package net.optebiz.crudapi.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.optebiz.crudapi.controller.TblOpsReqController;
import net.optebiz.crudapi.model.TblOpsReq;

@Repository
public class TblOpsReqDao {
	
	@Autowired(required=true)
	JdbcTemplate jdbcTemplate;

	
	
	/* private NamedParameterJdbcTemplate namedParameterJdbcTemplate; */

	private static final Logger logger = LoggerFactory.getLogger(TblOpsReqDao.class);

	private final ObjectMapper mapper = new ObjectMapper();
	
	public Object getTblOpsReqByAutoId(int autoId) {

		logger.info("DataDao getDataByAutoId = {}", autoId);
		String str = null;
		String query = "";
		List<Map<String, Object>> ls = null;
		if (autoId > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append("select AutoId ,Description,Id,Properties,ERPSeqNumber,Qty,UUId from tblOpsReq ");
			sb.append("where AutoId = ").append(autoId);

			query = sb.toString().intern();
			logger.debug("DataDao getDataByAutoId query = {}", query);
			ls = jdbcTemplate.queryForList(query);
			logger.debug("DataDao getDataByAutoId  ls = {}", ls);
		}
		try {
			if (ls.size() > 0) {
				str = mapper.writeValueAsString(ls);
				logger.debug("DataDao getDataByAutoId  str =  {}", str);
			}
		} catch (JsonProcessingException e) {

			logger.error("DataDao getDataByAutoId  cache block =  {}", e.getMessage());
		}
		return str;
	}

	public Object getTblLineDefByAutoId(int autoId) {

		logger.info("DataDao getTblLineDefByAutoId = {}", autoId);

		String str = null;
		String query = "";
		List<Map<String, Object>> ls = null;
		
		if (autoId > 0) 
		{
			StringBuilder sb = new StringBuilder();
			sb.append(" select tld.AutoID FROM tblLineDef tld INNER JOIN tblLineDefProp tldp ");
			sb.append(" ON tldp.tblLineDef_AutoID =").append("tld.AutoId WHERE equiplevel_AutoiD = ").append(autoId);
			query = sb.toString().intern();
			logger.debug("DataDao getTblLineDefByAutoId query = {}", query);
			ls = jdbcTemplate.queryForList(query);
			logger.debug("DataDao getTblLineDefByAutoId  ls = {}", ls);
		}
		try 
		{
			if (ls.size() > 0)
			{
				str = mapper.writeValueAsString(ls);
				logger.debug("DataDao getTblLineDefByAutoId  str =  {}", str);
			}
		} 
		catch (JsonProcessingException e)
		{

			logger.error("DataDao getTblLineDefByAutoId  cache block =  {}", e.getMessage());
		}
		return str;
	}

	public Object getTblOpsReqSeqByAutoId(int autoId) {

		logger.info("DataDao getTblOpsReqSeqByAutoId = {}", autoId);

		String query = "";
		List<Map<String, Object>> ls = null;
		if (autoId > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append("select AutoId, Description from tblOpsReqSeg where AutoId = ").append(autoId);
			query = sb.toString().intern();
			logger.debug("DataDao getTblOpsReqSeqByAutoId query = {}", query);
			ls = jdbcTemplate.queryForList(query);
			logger.debug("DataDao getTblOpsReqSeqByAutoId  ls = {}", ls);
		}
		String str = null;
		try {
			if (ls.size() > 0) {
				str = mapper.writeValueAsString(ls);
				logger.debug("DataDao getTblOpsReqSeqByAutoId  str =  {}", str);
			}
		} catch (JsonProcessingException e) {

			logger.error("DataDao getTblOpsReqSeqByAutoId  cache block =  {}", e.getMessage());
		}
		return str;
	}

	public Object getMatlDefPropByAutoId(int autoId) {

		logger.info("DataDao getMatlDefPropByAutoId = {}", autoId);

		String query = "";
		List<Map<String, Object>> ls = null;
		if (autoId > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT * FROM tblMatlDefProp WHERE tblMatlDef_AutoID = ").append(autoId).append(" ORDER BY AUtoID");
			query = sb.toString().intern();

			logger.debug("DataDao getMatlDefPropByAutoId query = {}", query);

			ls = jdbcTemplate.queryForList(query);
			
			logger.debug("DataDao getMatlDefPropByAutoId  ls = {}", ls);
		}
		String str = null;
		try {
			if (ls.size() > 0) {
				str = mapper.writeValueAsString(ls);
				logger.debug("DataDao getMatlDefPropByAutoId  str =  {}", str);
			}
		} catch (JsonProcessingException e) {

			logger.error("DataDao getMatlDefPropByAutoId  cache block =  {}", e.getMessage());
		}
		return str;
	}
	
	public Object getAutoidFromOpsReqTbl() {

		logger.info("DataDao getAutoidFromOpsReqTbl");

		String query = "";
		List<Map<String, Object>> ls = null;
		
			
			StringBuilder sb = new StringBuilder();
			
			sb.append("SELECT tor.AutoID FROM tblOPSREQ tor INNER JOIN tblOSPREQSeg tors ON tors.tblOPSREQ_AutoID= tor.AutoID");
			query = sb.toString().intern();
			
			logger.debug("DataDao getAutoidFromOpsReqTbl query = {}", query);
			
			ls = jdbcTemplate.queryForList(query);
			logger.debug("DataDao getAutoidFromOpsReqTbl  ls = {}", ls);
		
		String str = null;
		try {
			if (ls.size() > 0) {
				str = mapper.writeValueAsString(ls);
				logger.debug("DataDao getAutoidFromOpsReqTbl  str =  {}", str);
			}
		} catch (JsonProcessingException e) {

			logger.error("DataDao getAutoidFromOpsReqTbl  cache block =  {}", e.getMessage());
		}
		return str;
	}
	
	
	
	
	
	/*
	 * public List<TblOpsReq> getDataByAutoIdWithParameter(int autoId) {
	 * 
	 * final Map<String, Object> params = new HashMap<>(); params.put("autoId",
	 * autoId); return this.namedParameterJdbcTemplate
	 * .query(" select AutoId ,Description,Id,Properties,ERPSeqNumber,Qty,UUId from tblOpsReq"
	 * + " where AutoId = :autoId", params, new
	 * BeanPropertyRowMapper<TblOpsReq>(TblOpsReq.class));
	 * 
	 * }
	 */

}