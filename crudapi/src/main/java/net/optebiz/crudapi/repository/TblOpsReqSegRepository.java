package net.optebiz.crudapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import net.optebiz.crudapi.model.TblOpsReqSeg;

@Repository
public interface TblOpsReqSegRepository extends JpaRepository<TblOpsReqSeg,Integer>{
@Query(nativeQuery = true)
public List<TblOpsReqSeg> getTblOpsSeqByAutoId(int AutoId);



}
