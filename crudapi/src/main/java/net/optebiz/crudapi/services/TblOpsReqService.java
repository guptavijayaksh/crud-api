package net.optebiz.crudapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import net.optebiz.crudapi.dao.TblOpsReqDao;
import net.optebiz.crudapi.model.TblOpsReq;
import net.optebiz.crudapi.model.TblOpsReqSeg;
import net.optebiz.crudapi.repository.TblOpsReqRepository;
import net.optebiz.crudapi.repository.TblOpsReqSegRepository;

@Service
public class TblOpsReqService implements TblOpsReqInterface {

//	 @Autowired
//	 private TblOpsReqRepository tblOpsReqRepository;
	 
	 @Autowired
	 private  TblOpsReqDao  dataDao;
	 
//	 @Autowired
//	 private  TblOpsReqSegRepository  tblOpsReqSegRepository; 
	 
	
//	@Override
//	public Optional<TblOpsReq> getTblOpsReqById(int autoId) {
//		
//		return tblOpsReqRepository.findById(autoId);
//	}

//	@Override
//	public Optional<TblOpsReqSeg> getTblOpsSeqByAutoId(int autoId) {
//		return tblOpsReqSegRepository.findById(autoId);
//	}
	
	@Override
	public Object getTblOpsReqByAutoId(int autoId) {
		return dataDao.getTblOpsReqByAutoId(autoId);
	}
	
	
	@Override
	public Object getTblLineDefByAutoId(int autoId) {
		return dataDao.getTblLineDefByAutoId(autoId);
	}
	
	@Override
	public Object getTblOpsReqSeqByAutoId(int autoId) {
		return dataDao.getTblOpsReqSeqByAutoId(autoId);
	}
	
	
	@Override
	public Object getMatlDefPropByAutoId(int autoId) {
		return dataDao.getMatlDefPropByAutoId(autoId);
	}
	
	
	@Override
	public Object getAutoidFromOpsReqTbl() {
		return dataDao.getAutoidFromOpsReqTbl();
	}
	
//	@Override
//	public List<TblOpsReq> getDataServiceByAutoIdWithParam(int autoId) {
//		return dataDao.getDataByAutoIdWithParameter(autoId);
//	}
	
	
}
